const mdp = document.getElementById("mdp");
// const str = document.getElementById("mdp").value;

const example = document.querySelector(".example");
const equal = document.querySelector(".equal");
const msgMdp = document.querySelector(".msgMdp");
const form = document.querySelector(".register-form");

const mail = document.getElementById("mail");
const mailC = document.getElementById("mailC");

mail.addEventListener("input", (e) => {
  val = e.target.value;
});

mailC.addEventListener("input", (e) => {
  valC = e.target.value;
});

mdp.addEventListener("input", (e) => {
  str = e.target.value;
  console.log(str);
});

//----------------- Contrôle mot de passe et e-mail----------------------//
form.addEventListener("submit", (e) => {
  if (val != valC) {
    e.preventDefault();
    equal.classList.add("show");
  }
  if (
    !str.match(/[0-9]/g) ||
    !str.match(/[A-Z]/g) ||
    !str.match(/[a-z]/g) ||
    !str.match(/[^a-zA-Z\d]/g) ||
    str.length < 12
  ) {
    e.preventDefault();
    msgMdp.classList.add("show");
  }
});

mdp.addEventListener("click", (e) => {
  example.classList.add("show");
});

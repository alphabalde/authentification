<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="css/design.css" />

    <title> Accueil </title>
</head>

<body>
    <?php
    if (isset($_POST["login"])) { ?>
        <!-- Message affiché quand l'utilisateur est connecté -->
        <div class="centered-div">
            <h1>On vous souhaite la bienvenue ! </h1>
            <a class="btn" href="./?action=connexion">Retour</a>
        </div>
        <?php
    } elseif (isset($_POST["prenom"])) {
        if ($unMail == $unMailC) { ?>
            <!-- Message affiché quand l'utilisateur est inscrit -->
            <div class="centered-div">
                <h3>Nous avons bien reçu votre demande d'inscription. Un message de confirmation vient de vous être envoyé par mail. </h3>
                <a class="btn" href="./?action=connexion">Retour</a>
            </div>
    <?php
        }
    } ?>

</body>

</html>
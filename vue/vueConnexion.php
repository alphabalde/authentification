         <!DOCTYPE html>
         <html lang="fr">

         <head>
         	<meta charset="utf-8" />
         	<link rel="stylesheet" href="css/design.css" />
         	<title> Connexion </title>
         </head>

         <body>
         	<div class="container-main">
         		<!-- Conteneur principal -->
         		<div class="cnx-form-container">
         			<!-- Conteneur du formulaire de connexion-->

         			<form action="./?action=accueil" class="cnx-form" method="post">

         				<div class="label-input">
         					<!-- Classe qui met le label au dessus du input -->
         					<label for="login"> Adresse e-mail</label>
         					<input type="text" name="login" placeholder="Votre login">
         					<?php if (isset($_POST["login"])) {
									if ($client == null) { ?>
         							<p>Login ou mot de passe incorrect !</p>
         					<?php }
								} ?>
         				</div>

         				<div class="label-input">
         					<label for="password"> Mot de passe</label>
         					<input type="password" name="mdp" placeholder="Votre mot de passe">
         				</div>

         				<input type="submit" value="Connexion">
         			</form>

         		</div>

         		<div class="register-form-container">
         			<!-- Conteneur du formulaire d'inscription -->

         			<h1>Inscription</h1>
         			<h3>C'est gratuit (et ça le restera toujours)</h3>

         			<form action="./?action=accueil" class="register-form" method="post">

         				<div class="side-by-side-input">
         					<input type="text" name="prenom" placeholder="Prénom" required>
         					<input type="text" name="nom" placeholder="Nom de famille" required>
         				</div>

         				<input type="text" id="mail" name="mail" placeholder="Entrez votre email" required>
         				<input type="text" id="mailC" name="mailC" placeholder="Confirmez votre email" required>
         				<div class="equal">Attention les adresses mails saisies doivent être identiques !</div>
         				<input type="text" id="mdp" name="nouveauMdp" placeholder="Nouveau mot de passe" required>
         				<div class="example">Le mot de passe doit comporter au minimum 12 caractères, une minuscule, un majuscule, un chiffre et un caractère spécial !</div>
         				<div class="msgMdp">Le mot de passe saisi est incorrect !</div>
         				<?php if (isset($_POST["prenom"])) {
								if ($client != null) { ?>
         						<p id="msgAccount">L'adresse e-mail saisie est associée à un compte existant ! </p>
         				<?php
								}
							}
							?>
         				<div class="birth-part">
         					<h4>Date de naissance</h4>
         					<div class="birth-part-input">
         						<div style="width:47%;">
         							<input type="number" name="jour" placeholder="Jour" min="1" max="31" required>
         							<input type="number" name="mois" placeholder="Mois" min="1" max="12" required>
         							<input type="number" name="annee" placeholder="Année" min="1900" max="2011" required>
         						</div>

         					</div>

         				</div>

         				<div class="gender-part">

         					<div>
         						<input type="radio" name="genre" value="Femme" checked>
         						<label for="genre"> Femme </label>
         					</div>

         					<div>
         						<input type="radio" name="genre" value="Homme">
         						<label for="genre"> Homme </label>
         					</div>

         				</div>

         				<input type="submit" value="Inscription">
         			</form>
         			<br><br>
         		</div>
         	</div>

         	<script src="./JS/app.js"></script>
         </body>

         </html>
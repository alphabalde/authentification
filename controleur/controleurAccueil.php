<?php

include_once "$racine/modele/clientDAO.php";
include_once "$racine/modele/connexionClient.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';



/////////////////////////////////////////////////////////////CONNEXION//////////////////////////////////////////////////////////////////////////////
//Récupération des donnéees du formulaire de connexion 
if (isset($_POST["login"]) && isset($_POST["mdp"])) {
    $login = $_POST["login"];
    $mdp = md5($_POST["mdp"]);

    // Appel de la fonction permettant de charger un client
    $client = clientDAO::getClientByloginPassword($login, $mdp);

    // traiement de la connexion   
    if ($client != null) {
        connexionClient::login($login, $mdp);

        // Appel du script de la vue accueil
        include "$racine/vue/vueAccueil.php";
    } else {

        // Appel du script de la vue connexion
        include "$racine/vue/vueConnexion.php";
    }
}

////////////////////////////////////////////////////////////IBSCRIPTION/////////////////////////////////////////////////////////////////////////////
// 
elseif (
    isset($_POST["prenom"]) && isset($_POST["nom"]) && isset($_POST["mail"]) && isset($_POST["mailC"])
    && isset($_POST["nouveauMdp"]) && isset($_POST["jour"]) && isset($_POST["mois"]) && isset($_POST["annee"])
) {

    $unPrenom = $_POST["prenom"];
    $unNom = $_POST["nom"];
    $unMail = $_POST["mail"];
    $unMailC = $_POST["mailC"];

    $mdp = $_POST["nouveauMdp"];
    if (strlen($mdp) >= 12  && preg_match("/[a-z]/", $mdp) && preg_match("/[A-Z]/", $mdp) && preg_match("/[0-9]/", $mdp) && preg_match("/\W/", $mdp)) {
        $unNouveauMdp = md5($_POST["nouveauMdp"]);
    } else $unNouveauMdp = null;

    $unJour = $_POST["jour"];
    $unMois = $_POST["mois"];
    $uneAnnee = $_POST["annee"];
    $unGenre = $_POST["genre"];
    $uneDate = $uneAnnee . "-" . $unMois . "-" . $unJour;

    $client = clientDAO::getClientBylogin($unMail);

    if ($unMail == $unMailC && $unNouveauMdp != null && $client == null) {
        // Appel de la fonction permmettant d'ajouter un client   
        $client = clientDAO::addClient($unPrenom, $unNom, $unMail, $unNouveauMdp, $uneDate, $unGenre);

        $mail = new PHPMailer(True);

        try {
            $mail->isSMTP();                                            //Send using SMTP
            $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
            $mail->Username   = 'alphabalde0102@gmail.com';                     //SMTP username
            $mail->Password   = 'Jefe.0312';                               //SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
            $mail->Port       = 465;

            //Recipients
            $mail->setFrom('alphabalde0102@gmail.com', 'Motor Digital Publishing');
            $mail->addAddress($unMail, $unPrenom . " " . $unNom);
            $mail->addReplyTo('alphabalde0102@gmail.com', 'Information');

            //Content
            $mail->isHTML(true);
            $mail->Subject = 'Inscription';
            $mail->Body    = 'Bienvenue chez Motor Digital Publishing, votre inscription a été enregistrée.<br><br>
            Cordialement,<br><br>
            <b>Alpha BALDE</b> ';
            $mail->AltBody = 'Bienvenue chez Motor Digital Publishing, votre inscription a été enregistrée.

            Cordialement,

            ALpha BALDE';

            $mail->send();
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }

        //  Appel du script de la vue accueil
        include "$racine/vue/vueAccueil.php";
    } else {
        // Appel du script de la vue connexion
        include "$racine/vue/vueConnexion.php";
    }
}

<?php

function controleurPrincipal($action)
{

    $lesActions = array();
    $lesActions["default"] = "controleurConnexion.php";
    $lesActions["connexion"] = "controleurConnexion.php";
    $lesActions["accueil"] = "controleurAccueil.php";

    if (array_key_exists($action, $lesActions)) {
        return $lesActions[$action];
    } else {
        return $lesActions["default"];
    }
}

<?php

class client
{
    private $id;
    private $nom;
    private $prenom;
    private $login;
    private $motPasse;
    private $dateNaissance;
    private $genre;

    public function __construct($unId, $unNom, $unPrenom, $unlogin, $unMotPasse, $uneDateNaissance, $unGenre)
    {
        $this->id = $unId;
        $this->nom = $unNom;
        $this->prenom = $unPrenom;
        $this->login = $unlogin;
        $this->motPasse = $unMotPasse;
        $this->dateNaissance = $uneDateNaissance;
        $this->genre = $unGenre;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function getlogin()
    {
        return $this->login;
    }

    public function getMotPass()
    {
        return $this->motPasse;
    }

    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    public function getGenre()
    {
        return $this->genre;
    }
}

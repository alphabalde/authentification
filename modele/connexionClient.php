<?php
include_once "connexionPDO.php";
include_once "clientDAO.php";

class connexionClient
{

    public static function login($login, $motPasse)
    {
        if (!isset($_SESSION)) {
            session_start();
        }

        $user = clientDAO::getClientByloginPassword($login, $motPasse);
        $leLogin = $user->getlogin();
        $leMdp = $user->getMotPass();

        if ($leLogin == $login && $motPasse == $leMdp) {
            $_SESSION["login"] = $login;
            $_SESSION["motPasse"] = $motPasse;
        }
    }
}

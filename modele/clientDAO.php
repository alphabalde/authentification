<?php

include_once("connexionPDO.php");
include_once("client.php");

class clientDAO
{

    public static function getClients()
    {
        $resultat = array();
        $connexion = connexionPDO();
        $request = $connexion->prepare("select * from clients");
        $request->execute();
        $ligne = $request->fetchAll();

        foreach ($ligne as $value) {
            $client = new client($value['id'], $value['nom'], $value['prenom'], $value['login'], $value['motPasse'], $value['dateNaissance'], $value['genre']);

            $resultat[] = $client;
        }

        return $resultat;
    }


    public static function getClientByloginPassword($login, $mdp)
    {
        $connexion = connexionPDO();
        $request = $connexion->prepare("select* from clients where login = :login and motPasse = :motPasse");
        $request->bindValue(':login', $login, PDO::PARAM_STR);
        $request->bindValue(':motPasse', $mdp, PDO::PARAM_STR);
        $request->execute();

        $ligne = $request->fetch(PDO::FETCH_ASSOC);
        if ($ligne != null) {
            $client = new client($ligne['id'], $ligne['nom'], $ligne['prenom'], $ligne['login'], $ligne['motPasse'], $ligne['dateNaissance'], $ligne['genre']);
            return $client;
        } else {
            return null;
        }
    }


    public static function getClientBylogin($login)
    {
        $connexion = connexionPDO();
        $request = $connexion->prepare("select* from clients where login = :login");
        $request->bindValue(':login', $login, PDO::PARAM_STR);
        $request->execute();
        $ligne = $request->fetch(PDO::FETCH_ASSOC);
        if ($ligne != null) {
            $client = new client($ligne['id'], $ligne['nom'], $ligne['prenom'], $ligne['login'], $ligne['motPasse'], $ligne['dateNaissance'], $ligne['genre']);
            return $client;
        } else {
            return null;
        }
    }


    public static function addClient($prenom, $nom, $mail, $mdp, $date, $genre)
    {
        $connex = connexionPDO();
        $request = $connex->prepare(" insert into clients(prenom, nom, login, motPasse, dateNaissance, genre) 
                                      values(:prenom, :nom, :login, :motPasse, :dateNaissance, :genre)");
        $request->bindValue(':prenom', $prenom, PDO::PARAM_STR);
        $request->bindValue(':nom', $nom, PDO::PARAM_STR);
        $request->bindValue(':login', $mail, PDO::PARAM_STR);
        $request->bindValue(':motPasse', $mdp, PDO::PARAM_STR);
        $request->bindValue(':dateNaissance', $date, PDO::PARAM_STR);
        $request->bindValue(':genre', $genre, PDO::PARAM_STR);

        $request->execute();
    }
}

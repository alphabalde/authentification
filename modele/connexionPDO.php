<?php

function connexionPDO()
{
    $login = "root";
    $mdp = "root";
    $bd = "client_db";
    $serveur = "localhost";

    try {
        $connexion = new PDO("mysql:host=$serveur;dbname=$bd", $login, $mdp, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $connexion;
    } catch (PDOException $e) {
        print "Erreur de connexion PDO ";
        die();
    }
}
